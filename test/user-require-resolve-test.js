'use strict'

const {
  after,
  assert,
  before,
  clearModule,
  describe,
  getFixtureBuilder,
  getFixture = getFixtureBuilder(__filename),
  it,
} = require('./harness')
const fsp = require('node:fs/promises')
const os = require('node:os')
const ospath = require('node:path')
const userRequire = require('@antora/user-require-helper')

const CWD = process.cwd()
const FIXTURES_DIR = getFixture()
const NODE_MODULES = module.paths[1]
const SAMPLE_NODE_MODULES = getFixture('..', 'sample_node_modules')
const WORK_DIR = ospath.join(__dirname, ospath.basename(__filename, '.js'))

describe('userRequire.resolve()', () => {
  const setUpNodeModules = () => fsp.cp(SAMPLE_NODE_MODULES, ospath.join(WORK_DIR, 'node_modules'), { recursive: true })
  const tearDownWorkDir = () => fsp.rm(WORK_DIR, { recursive: true, maxRetries: 10 })

  describe('argument validation', () => {
    it('should throw error if request is undefined', () => {
      const expected = new TypeError('The "request" argument must be of type string. Received type undefined')
      const actual = userRequire.resolve
      assert.throws(actual, expected)
    })

    it('should throw error if request is not a string', () => {
      const input = true
      const expected = new TypeError('The "request" argument must be of type string. Received type boolean')
      const actual = () => userRequire.resolve(input)
      assert.throws(actual, expected)
    })

    it('should throw error if request is empty', () => {
      const input = ''
      const expected = Object.assign(new Error(), {
        message: /^Cannot find module ''/,
        code: 'MODULE_NOT_FOUND',
      })
      const actual = () => userRequire.resolve(input)
      assert.throws(actual, expected)
    })

    it('should show userRequire.resolve function call in stack', () => {
      const input = ''
      const expected = Object.assign(new Error(), {
        message: /^Cannot find module ''/,
        stack: / at (?:Function\.)?userRequire\.resolve /,
      })
      const actual = () => userRequire.resolve(input)
      assert.throws(actual, expected)
    })
  })

  describe('as path', () => {
    describe('absolute', () => {
      it('should resolve absolute path', () => {
        const input = getFixture('script.js')
        const expected = input
        const actual = userRequire.resolve(input)
        assert.equal(actual, expected)
      })

      it('should throw error if absolute path cannot be found', () => {
        const input = getFixture('does-not-exist.js')
        const expected = Object.assign(new Error(), {
          message: new RegExp(`^Cannot find module '${RegExp.escape(input)}'`),
          code: 'MODULE_NOT_FOUND',
        })
        const actual = () => userRequire.resolve(input)
        assert.throws(actual, expected)
      })
    })

    describe('relative', () => {
      it('should resolve relative path from value of base argument', () => {
        const input = 'script.js'
        const expected = getFixture(input)
        const actual = userRequire.resolve(input, { base: FIXTURES_DIR })
        assert.equal(actual, expected)
      })

      it('should resolve relative path with leading parent refernece from value of base argument', () => {
        ;['/'].concat(ospath.sep === '/' ? [] : [ospath.sep]).forEach((sep) => {
          const input = `..${sep}script.js`
          const expected = getFixture(input.substr(3))
          const actual = userRequire.resolve(input, { base: getFixture('subfolder') })
          assert.equal(actual, expected)
        })
      })

      it('should throw error if relative path cannot be found', () => {
        const input = 'does-not-exist.js'
        const expected = Object.assign(new Error(), {
          message: new RegExp(`^Cannot find module '${RegExp.escape(getFixture(input))}'`),
        })
        const actual = () => userRequire.resolve(input, { base: FIXTURES_DIR })
        assert.throws(actual, expected)
      })

      it('should throw error if relative path cannot be found because base argument not specified', () => {
        const input = 'script.js'
        const expected = Object.assign(new Error(), {
          message: new RegExp(`^Cannot find module '${RegExp.escape(ospath.join(CWD, input))}'`),
        })
        const actual = () => userRequire.resolve(input)
        assert.throws(actual, expected)
      })

      it('should throw error if relative path is missing a file extension', () => {
        const input = 'script'
        const expected = Object.assign(new Error(), {
          message: new RegExp(`^Cannot find module '${RegExp.escape(input)}'`),
        })
        const actual = () => userRequire.resolve(input, { base: FIXTURES_DIR })
        assert.throws(actual, expected)
      })
    })

    describe('dot relative', () => {
      it('should resolve dot relative path from value of dot argument', () => {
        ;['/'].concat(ospath.sep === '/' ? [] : [ospath.sep]).forEach((sep) => {
          const input = `.${sep}script.js`
          const expected = getFixture(input.substr(2))
          const actual = userRequire.resolve(input, { dot: FIXTURES_DIR })
          assert.equal(actual, expected)
        })
      })

      it('should interpret dot relative path without file extension as path', () => {
        ;['/'].concat(ospath.sep === '/' ? [] : [ospath.sep]).forEach((sep) => {
          const input = `.${sep}script`
          const expected = getFixture(input.substr(2) + '.js')
          const actual = userRequire.resolve(input, { dot: FIXTURES_DIR })
          assert.equal(actual, expected)
        })
      })

      it('should interpret dot relative path without file extension in folder as path', () => {
        const fixturesFolder = ospath.basename(FIXTURES_DIR)
        const dot = ospath.dirname(FIXTURES_DIR)
        const input = `./${fixturesFolder}/script`
        const expected = ospath.join(dot, input.substr(2) + '.js')
        const actual = userRequire.resolve(input, { dot })
        assert.equal(actual, expected)
      })

      it('should throw error if dot relative path cannot be found', () => {
        const input = './does-not-exist.js'
        const expected = Object.assign(new Error(), {
          message: new RegExp(`^Cannot find module '${RegExp.escape(getFixture(input.substr(2)))}'`),
        })
        const actual = () => userRequire.resolve(input, { dot: FIXTURES_DIR })
        assert.throws(actual, expected)
      })

      it('should throw error if dot relative path cannot be found because dot argument not specified', () => {
        const input = './script.js'
        const expected = Object.assign(new Error(), {
          message: new RegExp(`^Cannot find module '${RegExp.escape(ospath.join(CWD, input.substr(2)))}'`),
        })
        const actual = () => userRequire.resolve(input)
        assert.throws(actual, expected)
      })
    })

    describe('cwd relative', () => {
      before(() => process.chdir(FIXTURES_DIR))

      after(() => process.chdir(CWD))

      it('should resolve cwd relative path from cwd', () => {
        const input = '~+/script.js'
        const expected = getFixture(input.substr(3))
        const actual = userRequire.resolve(input)
        assert.equal(actual, expected)
        assert.equal(process.cwd(), FIXTURES_DIR)
      })

      it('should allow cwd to be overridden using cwd argument', () => {
        const input = '~+/' + ospath.basename(__filename)
        const cwd = __dirname
        const expected = ospath.join(cwd, input.substr(3))
        const actual = userRequire.resolve(input, { cwd })
        assert.equal(actual, expected)
        assert.equal(process.cwd(), FIXTURES_DIR)
      })

      it('should throw error if cwd relative path cannot be found', () => {
        const input = '~+/does-not-exist.js'
        const expected = Object.assign(new Error(), {
          message: new RegExp(`^Cannot find module '${RegExp.escape(getFixture(input.substr(3)))}'`),
        })
        const actual = () => userRequire.resolve(input)
        assert.throws(actual, expected)
      })
    })

    describe('~ relative', () => {
      const oldEnv = { ...process.env }

      before(() => Object.assign(process.env, { HOME: FIXTURES_DIR, USERPROFILE: FIXTURES_DIR }))

      after(() => {
        for (const name of Object.keys(process.env)) delete process.env[name]
        Object.assign(process.env, oldEnv)
      })

      it('should resolve ~ relative path from home directory', () => {
        const input = '~/script.js'
        const expected = getFixture(input.substr(2))
        const actual = userRequire.resolve(input)
        assert.equal(actual, expected)
        assert.equal(os.homedir(), FIXTURES_DIR)
      })

      it('should throw error if cwd relative path cannot be found', () => {
        const input = '~/does-not-exist.js'
        const expected = Object.assign(new Error(), {
          message: new RegExp(`^Cannot find module '${RegExp.escape(getFixture(input.substr(2)))}'`),
        })
        const actual = () => userRequire.resolve(input)
        assert.throws(actual, expected)
      })
    })
  })

  describe('as module name', () => {
    describe('absolute', () => {
      it('should resolve absolute module name', () => {
        const input = ospath.join(NODE_MODULES, '@antora', 'expand-path-helper')
        const expected = require.resolve('@antora/expand-path-helper')
        const actual = userRequire.resolve(input)
        assert.equal(actual, expected)
      })

      it('should throw error if absolute module name cannot be found', () => {
        const input = ospath.join(NODE_MODULES, 'does-not-exist')
        const expected = Object.assign(new Error(), {
          message: new RegExp(`^Cannot find module '${RegExp.escape(input)}'`),
          code: 'MODULE_NOT_FOUND',
        })
        const actual = () => userRequire.resolve(input)
        assert.throws(actual, expected)
      })
    })

    describe('relative', () => {
      before(setUpNodeModules)

      after(tearDownWorkDir)

      it('should resolve relative module name from current project', () => {
        const input = '@antora/expand-path-helper'
        const expected = require.resolve(input)
        const actual = userRequire.resolve(input)
        assert.equal(actual, expected)
      })

      it('should resolve relative module name from calling script', () => {
        const expected = require.resolve('is-number-type', { paths: [WORK_DIR] })
        const resolvedModulePath = require.resolve('resolve-from-module', { paths: [WORK_DIR] })
        clearModule(resolvedModulePath)
        const actual = require(resolvedModulePath)()
        assert.equal(actual, expected)
      })

      it('should throw error if relative module name cannot be found', () => {
        const input = 'does-not-exist'
        const expected = Object.assign(new Error(), {
          message: new RegExp(`^Cannot find module '${RegExp.escape(input)}'`),
        })
        const actual = () => userRequire.resolve(input)
        assert.throws(actual, expected)
      })

      it('should interpret path with file extension as module name if it ends with /', () => {
        const input = 'fizzbuzz.js/'
        const expected = require.resolve('fizzbuzz.js', { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { paths: [WORK_DIR] })
        assert.equal(actual, expected)
      })
    })

    describe('relative to context', () => {
      before(setUpNodeModules)

      after(tearDownWorkDir)

      it('should resolve relative module name from resolved context', () => {
        const workFolder = ospath.basename(WORK_DIR)
        const input = `./${workFolder}:is-number-type`
        const expected = require.resolve(input.substr(workFolder.length + 3), { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { dot: ospath.dirname(WORK_DIR) })
        assert.equal(actual, expected)
      })

      it('should resolve relative module name with leading parent reference from resolved context', () => {
        const workFolder = ospath.basename(WORK_DIR)
        const input = `../${workFolder}:is-number-type`
        const expected = require.resolve(input.substr(workFolder.length + 4), { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { base: WORK_DIR })
        assert.equal(actual, expected)
      })

      it('should not intercept the built-in node: prefix', () => {
        const input = 'node:fs'
        const expected = input
        const actual = userRequire.resolve(input)
        assert.equal(actual, expected)
      })

      it('should throw error if relative module name cannot be found at resolved context', () => {
        const workFolder = ospath.basename(WORK_DIR)
        const input = `./${workFolder}:does-not-exist`
        const expected = Object.assign(new Error(), {
          message: new RegExp(
            `^Cannot find module '${RegExp.escape(input.substr(workFolder.length + 3))}'` +
              ` at path ${RegExp.escape(WORK_DIR)}`
          ),
          code: 'MODULE_NOT_FOUND',
        })
        const actual = () => userRequire.resolve(input, { dot: ospath.dirname(WORK_DIR) })
        assert.throws(actual, expected)
      })

      it('should ignore specified paths when resolving relative module name from resolved context', () => {
        const workFolder = ospath.basename(WORK_DIR)
        const input = `./${workFolder}:is-number-type`
        const expected = require.resolve(input.substr(workFolder.length + 3), { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { dot: ospath.dirname(WORK_DIR), paths: [FIXTURES_DIR] })
        assert.equal(actual, expected)
      })
    })

    describe('strict relative', () => {
      before(setUpNodeModules)

      after(tearDownWorkDir)

      it('should resolve strict relative module name from base argument', () => {
        const input = ':is-number-type'
        const expected = require.resolve('is-number-type', { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { base: WORK_DIR })
        assert.equal(actual, expected)
      })

      it('should resolve strict relative module name from base argument that uses path shorthand', () => {
        try {
          process.chdir(WORK_DIR)
          const input = ':is-number-type'
          const expected = require.resolve('is-number-type', { paths: [WORK_DIR] })
          const actual = userRequire.resolve(input, { base: '~+' })
          assert.equal(actual, expected)
        } finally {
          process.chdir(CWD)
        }
      })

      it('should resolve strict relative module name from cwd if base argument not specified', () => {
        try {
          process.chdir(WORK_DIR)
          const input = ':is-number-type'
          const expected = require.resolve(input.substr(1), { paths: [WORK_DIR] })
          const actual = userRequire.resolve(input)
          assert.equal(actual, expected)
        } finally {
          process.chdir(CWD)
        }
      })

      it('should interpret path with file extension and anchor as strict relative module name if it ends with /', () => {
        const input = ':is-number-type/index.js/'
        const expected = require.resolve('is-number-type', { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { base: WORK_DIR })
        assert.equal(actual, expected)
      })

      it('should throw error if strict relative module name cannot be found', () => {
        const input = ':does-not-exist'
        const expected = Object.assign(new Error(), {
          message: new RegExp(
            `^Cannot find module '${RegExp.escape(input.substr(1))}' at path ${RegExp.escape(WORK_DIR)}`
          ),
        })
        const actual = () => userRequire.resolve(input, { base: WORK_DIR })
        assert.throws(actual, expected)
      })

      it('should ignore specified paths when resolving strict relative module name', () => {
        const input = ':is-number-type'
        const expected = require.resolve(input.substr(1), { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { base: WORK_DIR, paths: [FIXTURES_DIR] })
        assert.equal(actual, expected)
      })
    })

    describe('calling script relative', () => {
      before(async () => {
        await setUpNodeModules()
        process.chdir(WORK_DIR)
      })

      after(async () => {
        process.chdir(CWD)
        await tearDownWorkDir()
      })

      it('should resolve calling script relative module name', () => {
        const input = '^:@antora/expand-path-helper'
        const expected = require.resolve(input.substr(2))
        const actual = userRequire.resolve(input, { paths: [WORK_DIR] })
        assert.equal(actual, expected)
      })

      it('should throw error if calling script relative module name cannot be found', () => {
        const input = '^:does-not-exist'
        const expected = Object.assign(new Error(), {
          message: new RegExp(`^Cannot find module '${RegExp.escape(input.substr(2))}'`),
        })
        const actual = () => userRequire.resolve(input, { paths: [WORK_DIR] })
        assert.throws(actual, expected)
      })

      it('should ignore specified paths when resolving calling script relative module name', () => {
        const input = '^:@antora/expand-path-helper'
        const expected = require.resolve(input.substr(2))
        const actual = userRequire.resolve(input, { paths: [WORK_DIR] })
        assert.equal(actual, expected)
      })
    })

    describe('dot relative', () => {
      before(setUpNodeModules)

      after(tearDownWorkDir)

      it('should resolve dot relative module name from dot argument', () => {
        const input = '.:is-number-type'
        const expected = require.resolve(input.substr(2), { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { dot: WORK_DIR })
        assert.equal(actual, expected)
      })

      it('should resolve dot relative module name from dot argument that uses path shorthand', () => {
        try {
          process.chdir(WORK_DIR)
          const input = '.:is-number-type'
          const expected = require.resolve(input.substr(2), { paths: [WORK_DIR] })
          const actual = userRequire.resolve(input, { dot: '.' })
          assert.equal(actual, expected)
        } finally {
          process.chdir(CWD)
        }
      })

      it('should resolve dot relative module name from cwd if dot argument not specified', () => {
        try {
          process.chdir(WORK_DIR)
          const input = '.:is-number-type'
          const expected = require.resolve(input.substr(2), { paths: [WORK_DIR] })
          const actual = userRequire.resolve(input)
          assert.equal(actual, expected)
        } finally {
          process.chdir(CWD)
        }
      })

      it('should throw error if dot relative module name cannot be found', () => {
        const input = '.:does-not-exist'
        const expected = Object.assign(new Error(), {
          message: new RegExp(
            `^Cannot find module '${RegExp.escape(input.substr(2))}' at path ${RegExp.escape(WORK_DIR)}`
          ),
        })
        const actual = () => userRequire.resolve(input, { dot: WORK_DIR })
        assert.throws(actual, expected)
      })

      it('should ignore specified paths when resolving dot relative module name', () => {
        const input = '.:is-number-type'
        const expected = require.resolve(input.substr(2), { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { dot: WORK_DIR, paths: [FIXTURES_DIR] })
        assert.equal(actual, expected)
      })
    })

    describe('cwd relative', () => {
      before(async () => {
        await setUpNodeModules()
        process.chdir(WORK_DIR)
      })

      after(async () => {
        process.chdir(CWD)
        await tearDownWorkDir()
      })

      it('should resolve cwd relative module name from cwd', () => {
        const input = '~+:is-number-type'
        const expected = require.resolve(input.substr(3), { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input)
        assert.equal(actual, expected)
        assert.equal(process.cwd(), WORK_DIR)
      })

      it('should allow cwd to be overridden using cwd argument', () => {
        try {
          const projectRoot = ospath.resolve(__dirname, '..')
          process.chdir(projectRoot)
          const input = '~+:is-number-type'
          const cwd = WORK_DIR
          const expected = require.resolve(input.substr(3), { paths: [cwd] })
          const actual = userRequire.resolve(input, { cwd })
          assert.equal(actual, expected)
          assert.equal(process.cwd(), projectRoot)
        } finally {
          process.chdir(WORK_DIR)
        }
      })

      it('should throw error if dot relative module name cannot be found', () => {
        const input = '~+:does-not-exist'
        const expected = Object.assign(new Error(), {
          message: new RegExp(
            `^Cannot find module '${RegExp.escape(input.substr(3))}' at path ${RegExp.escape(process.cwd())}`
          ),
        })
        const actual = () => userRequire.resolve(input)
        assert.throws(actual, expected)
      })

      it('should ignore specified paths when resolving cwd relative module name', () => {
        const input = '~+:is-number-type'
        const expected = require.resolve(input.substr(3), { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { paths: [FIXTURES_DIR] })
        assert.equal(actual, expected)
      })
    })

    describe('~ relative', () => {
      const oldEnv = { ...process.env }

      before(async () => {
        await setUpNodeModules()
        process.chdir(WORK_DIR)
        Object.assign(process.env, { HOME: WORK_DIR, USERPROFILE: WORK_DIR })
      })

      after(async () => {
        process.chdir(CWD)
        await tearDownWorkDir()
        for (const name of Object.keys(process.env)) delete process.env[name]
        Object.assign(process.env, oldEnv)
      })

      it('should resolve ~ relative module name from home directory', () => {
        const input = '~:is-number-type'
        const expected = require.resolve(input.substr(2), { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input)
        assert.equal(actual, expected)
        assert.equal(os.homedir(), WORK_DIR)
      })

      it('should throw error if ~ relative module name cannot be found', () => {
        const input = '~:does-not-exist'
        const expected = Object.assign(new Error(), {
          message: new RegExp(
            `^Cannot find module '${RegExp.escape(input.substr(2))}' at path ${RegExp.escape(os.homedir())}`
          ),
        })
        const actual = () => userRequire.resolve(input)
        assert.throws(actual, expected)
      })
    })

    describe('paths', () => {
      before(setUpNodeModules)

      after(tearDownWorkDir)

      it('should allow paths argument to control search path for bare module name', () => {
        const input = 'is-number-type'
        const expected = require.resolve('is-number-type', { paths: [WORK_DIR] })
        const actual = userRequire.resolve(input, { paths: [WORK_DIR, module.paths[0]] })
        assert.equal(actual, expected)
      })

      it('should show all search paths in error if bare module name cannot be found', () => {
        const input = 'does-not-exist'
        const paths = [WORK_DIR, module.paths[0]]
        const search = paths.join(' or ')
        const expected = Object.assign(new Error(), {
          message: new RegExp(`^Cannot find module '${RegExp.escape(input)}' at path ${RegExp.escape(search)}`),
        })
        const actual = () => userRequire.resolve(input, { paths })
        assert.throws(actual, expected)
      })
    })

    describe('resolve function', () => {
      it('should use specified resolve function to resolve path', () => {
        const input = getFixture('script.js')
        let captured
        const resolve = (request) => (captured = request)
        const actual = userRequire.resolve(input, { resolve })
        const expected = captured
        assert.equal(actual, expected)
        assert.equal(input, expected)
      })

      it('should use specified resolve function to resolve module name', () => {
        const input = 'is-number-type'
        let captured
        const resolve = (request) => (captured = request)
        const actual = userRequire.resolve(input, { resolve })
        const expected = captured
        assert.equal(actual, expected)
        assert.equal(input, expected)
      })

      it('should use specified resolve function to resolve module name from context', () => {
        const input = '.:is-number-type'
        let captured
        const resolve = (...args) => (captured = args)[0]
        const actual = userRequire.resolve(input, { dot: FIXTURES_DIR, resolve })
        assert(Array.isArray(captured))
        const expected = captured[0]
        assert.equal(actual, expected)
        assert.equal(input.substr(2), expected)
        assert.equal(captured[1].paths.length, 1)
        // NOTE Node.js automatically looks in node_modules folder at specified path
        assert.equal(captured[1].paths[0], FIXTURES_DIR)
      })
    })
  })
})
