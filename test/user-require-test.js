'use strict'

const {
  after,
  assert,
  before,
  clearModule,
  describe,
  getFixtureBuilder,
  getFixture = getFixtureBuilder(__filename),
  it,
} = require('./harness')
const fsp = require('node:fs/promises')
const ospath = require('node:path')
const userRequire = require('@antora/user-require-helper')

const SAMPLE_NODE_MODULES = getFixture('..', 'sample_node_modules')
const WORK_DIR = ospath.join(__dirname, ospath.basename(__filename, '.js'))

describe('userRequire()', () => {
  before(() => fsp.cp(SAMPLE_NODE_MODULES, ospath.join(WORK_DIR, 'node_modules'), { recursive: true }))

  after(() => fsp.rm(WORK_DIR, { recursive: true, maxRetries: 10 }))

  it('should show userRequire and userRequire.resolve function calls in stack', () => {
    const input = ''
    const expected = Object.assign(new Error(), {
      message: /^Cannot find module ''/,
      stack: / at (?:Function\.)?resolve [\s\S]*? at (?:Function\.)?userRequire\.resolve /,
    })
    const actual = () => userRequire(input)
    assert.throws(actual, expected)
  })

  it('should resolve and require path', () => {
    const input = './script.js'
    clearModule(getFixture(input.substr(2)))
    const expected = 'script called!'
    const actual = userRequire(input, { dot: getFixture() })()
    assert.equal(actual, expected)
  })

  it('should resolve and require module name', () => {
    const input = '.:is-number-type'
    clearModule(require.resolve(input.substr(2), { paths: [WORK_DIR] }))
    const expected = true
    const actual = userRequire(input, { dot: WORK_DIR })(100)
    assert.equal(actual, expected)
  })

  it('should use specified resolve function to resolved id to require', () => {
    const input = '.:is-number-type'
    const resolvePaths = [WORK_DIR]
    clearModule(require.resolve(input.substr(2), { paths: resolvePaths }))
    let captured
    const resolve = (...args) => require.resolve(...(captured = args))
    const expected = true
    const actual = userRequire(input, { dot: resolvePaths[0], resolve })(100)
    assert.equal(actual, expected)
    assert.equal(captured[0], input.substr(2))
    assert.deepEqual(captured[1], { paths: resolvePaths })
  })

  it('should resolve built-in module for module name with node: prefix', () => {
    try {
      const input = 'node:fs'
      const expected = require('node:fs')
      require.cache.fs = { exports: {} }
      const actual = userRequire(input)
      assert.equal(actual, expected)
    } finally {
      delete require.cache.fs
    }
  })
})
