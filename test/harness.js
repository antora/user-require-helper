'use strict'

const assert = require('node:assert/strict')
const { after, before, describe, it } = require('node:test')
const ospath = require('node:path')

RegExp.escape = (str) => str.replace(/[.*[\](|)\\]/g, '\\$&')

function clearModule (filename) {
  const cacheEntry = require.cache[filename]
  if (!cacheEntry) return
  const cacheEntryParent = cacheEntry.parent
  if (cacheEntryParent) cacheEntryParent.children = cacheEntryParent.children.filter((it) => it.filename !== filename)
  const children = cacheEntry.children
  delete require.cache[filename]
  for (const child of children) clearModule(child.filename)
}

function getFixtureBuilder (testFilename) {
  const basePath = ospath.join(ospath.dirname(testFilename), 'fixtures', ospath.basename(testFilename, '.js'))
  return (...pathSegments) => ospath.join(basePath, ...pathSegments)
}

module.exports = { after, assert, before, clearModule, describe, getFixtureBuilder, it }
